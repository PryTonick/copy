import java.io.*;

/**
 * Класс Copy предназначен для копирования одного или нескольких файлов,
 * папок включая их содержимое, в том числе и вложенные папки.
 *
 * @author Горшков Н.О., 16IT18K
 */

public class Copy{
    /*
     * Исходный путь до файла/каталога
     */
    private File inputFile;
    /*
     * Конечный путь до места копирования
     */
    private File outFile;

    /**
     * Конструктор для копирования одного файла или каталога
     *
     * @param inputFile исходный файл
     * @param outFile   конечный файл
     */
    public Copy(File inputFile, File outFile) {
        this.inputFile = inputFile;
        this.outFile = outFile;
    }

    /**
     * Метод выполняет копирование одного файла.
     * Если файл отсутствует, то метод прекращает работу и выдает ошибку( Файл не найден ).
     *
     * @param inputFile - Входной путь до файла(Абсолютный).
     * @param outFile - Путь до места копирования(Абсолютный).
     * @return
     */
    public void copyFile(File inputFile, File outFile){
        if(inputFile.isFile()){
            //Записывает в переменную fileName имя Файла и его расширение с помощью метода getName().
            String fileName = inputFile.getName();

            try (
                    InputStream input = new BufferedInputStream(
                            new FileInputStream(inputFile));
                    OutputStream output = new BufferedOutputStream(
                            new FileOutputStream(outFile + "/" + fileName))) {

                byte[] buffer = new byte[1024];
                int lengthRead;
                while ((lengthRead = input.read(buffer)) > 0) {

                    output.write(buffer, 0, lengthRead);

                    /*Очищает выходной поток и заставляет любые буферизованные выходные байты быть записанными.
                     * использование flush(), когда вам нужно быть уверенным, что все ваши данные из буфера написаны.
                     */
                    output.flush();
                }
                input.close();
                output.close();
                System.out.println("Файл " + fileName + " скопирован успешно");
            }
            catch (Exception e){
                System.out.println(e);
            }

        }else {
            System.out.println("Файл не найден");

        }
    }

    /**
     * Метод копирования каталога с вложенными папками или файлами.
     * Если каталог отсутствует то будет выведена ошибка и прекращена работа метода.
     *
     * @param inputFile - Входной путь до каталога(Абсолютный).
     * @param outFile - Путь места копирования(Абсолютный).
     */
    public void copyFolder(File inputFile, File outFile){
        String fileName = inputFile.getName();
        if (inputFile.isDirectory()) {
            // Добавляем все файлы из папки в массив
            String files[] = inputFile.list();
            for (String file : files) {
                // Запоминаем файлы для копирования
                File srcFile = new File(inputFile, file);
                File destFile = new File(outFile + "/", file);
                // Рекурсивный вызов метода
                copyFolder(srcFile , destFile);
            }
        }
        else{
            try (
                    InputStream input = new BufferedInputStream(
                            new FileInputStream(inputFile));
                    OutputStream output = new BufferedOutputStream(
                            new FileOutputStream( outFile + "/"))) {

                byte[] buffer = new byte[1024];
                int lengthRead;
                while ((lengthRead = input.read(buffer)) > 0) {

                    output.write(buffer, 0, lengthRead);

                    /*Очищает выходной поток и заставляет любые буферизованные выходные байты быть записанными.
                     * использование flush(), когда вам нужно быть уверенным, что все ваши данные из буфера написаны.
                     */
                    output.flush();
                }
                input.close();
                output.close();
                System.out.println("Файл " + fileName + " скопирован успешно");
            }
            catch (Exception e){
                System.out.println(e);
            }
        }
    }
}