import java.io.*;
import java.util.Scanner;

/**
 * Класс Main служит как лаунчер(приложение) для запуска и обработки информации пользователя
 * Данные введенные со строк и передачу их в класс Copy, а также дальнейшую работу с ними.
 *
 * @author Горшков Н.О. , 16ИТ18К
 */
public class Main {

    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите путь до файла/каталога который вы хотите скопировать. Пример ввода: ../src/test.txt");
        String inputFile = scanner.nextLine();
        System.out.println("Введите путь в место куда вы хотите скопировать. Пример ввода: ../src/test/");
        String outputFile = scanner.nextLine();

        File inputfile = new File(inputFile);
        File outputfile = new File(outputFile);

        Copy copyfile = new Copy(inputfile, outputfile);

        copyfile.copyFile(inputfile, outputfile);
        copyfile.copyFolder(inputfile, outputfile);
    }
}